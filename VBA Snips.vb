'' REGEX Checker
Function RegSelectCells(rRange)
	Dim rRange As Range
	Dim rCell As Range

	Dim re As Object
	Set re = CreateObject("vbscript.regexp")
	With re
	  .Pattern = "^\d\d?:\d\d[aApP][mM] - \d\d?:\d\d[aApP][mM]$"
	  .Global = False
	  .IgnoreCase = False
	End With

	Set rRange = Range("A2", "G225") 'For explanation

	For Each rCell In rRange.Cells
		If re.Test(rCell) Then
			rCell.Select()
		Else
		End If
	Next rCell
End Function
'' End Regex Snip

'' Simple Replace
Sub Replacing()
Dim strIn, strReplace, strWith, strOut As String
strIn = "My Text"
strReplace = "M"
strWith = "T"
strOut=Replace(strIn, strReplace, strWith)
End Sub
'' End Replace Snip

'' Regex Test Snip
Public Function regReplace(strIn, strFind) As String
Dim strMatched as String
Dim reg As Object
Set reg = CreateObject("vbscript.regexp")
With reg
  .Pattern = strFind ' "^\d\d" => 90 in 90aas
  .Global = True 'all possible matches in a string Def:No
  .IgnoreCase = False
End With
If reg.Test(strIn) Then 
	strMatched = reg.Execute(strIn) 'Returns found string
	regReplace = reg.Replace(strFind,strWith) 'Retruns the string replaced
End Function
'' End Regex Test Snip

'' Cycle Through Range
Public Function getRangeContents(ByVal strName As String) As String

    Dim r, rCell As Range
    Set r = getRangeFromName(strName) 'or Set a Range
    Dim strList As String
    Dim intCount As Integer

    For Each rCell In r
        intCount = intCount + 1
        If intCount > 1 Then
            If rCell.Text <> Null Then
            strList = strList & "," & rCell.Text
            End If
        Else
            strList = rCell.Text
        End If
    Next
    getRangeContents = strList
End Function

'' End CycleRange
# Region Read online XML
Function GetCurrentVersionNumber() As String
    Dim doc As MSXML2.DOMDocument??  ' Where ?? is the version number, such as 30 or 60
    Set doc = New MSXML2.DOMDocument??
    doc.async = False
    doc.Load("http://mywebsite.com/currentversion.xml")
    GetCurrentVersionNumber = doc.SelectSingleNode("/AppData/Version").Text
End Function
# END Region Online XML
# Region: Write Local XML
Private Sub ExportVersion()
    Dim version As String, doc As MSXML2.DOMDocument
    ' Where ?? is the version number, such as 30 or 60
    Set doc = New MSXML2.DOMDocument
    ' Get local version number
    version = getRangeContents("REVISION_NUMBER")
    ' Load XML file
    doc.Load (ThisWorkbook.path & "\version.xml")
    ' Select Version node and write current version number
    doc.SelectSingleNode("/AppData/Version").Text = version
    'Save the modified file
    doc.Save (ThisWorkbook.path & "\version.xml")
End Sub
# END Region

'~~> Get Users Temp Path
Function TempPath() As String
    TempPath = String$(MAX_PATH, Chr$(0))
    GetTempPath MAX_PATH, TempPath
    TempPath = Replace(TempPath, Chr$(0), "")
End Function
# End Region
# Region: Write to Text File
Private Sub ExportVersion()
    Dim version As String
    Dim VerFile, fnum1
    version = Right(getRangeContents("REVISION_NUMBER"), 7)
    
    VerFile = ThisWorkbook.path & "\version.txt"
    fnum1 = FreeFile()
    
    Open VerFile For Output As fnum1
        Print #fnum1, version
    Close #fnum1
End Sub
# End Region
# Region: Find ExactWord
Function ExactWordInString(Text As String, Word As String) As Boolean
  ExactWordInString = " " & UCase(Text) & " " Like "*[!A-Z]" & UCase(Word) & "[!A-Z]*"
End Function
# End Region
# Region: Maximize with multiple monitors
Private Const SM_CXVIRTUALSCREEN = 78
Private Const SM_CYVIRTUALSCREEN = 79

Private Declare Function GetSystemMetrics Lib "user32" ( _
ByVal nIndex As Long) As Long

Sub FillVirtualScreen()
    With Application
        .WindowState = xlNormal
        .Left = 0
        .Top = 0
        .Width = GetSystemMetrics(SM_CXVIRTUALSCREEN)
        .Height = GetSystemMetrics(SM_CYVIRTUALSCREEN)
    End With
End Sub
# End Region
