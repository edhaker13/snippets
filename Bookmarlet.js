    // This is the script for the GA proxy, single line for bookmarklet
    javascript:(function(){javascript:location.href='https://edproxy1.appspot.com/?url='+location.href.replace(/^https?:\/\//,'')})();
    // Broken out for better understanding
    javascript:(
    function()
    {
    javascript:location.href='https://edproxy1.appspot.com/?url='
    +location.href.replace(/^https?:\/\//,'')
    }
    )();
    //This one is the initial script for Glype VPS proxy
    javascript:(function(){javascript:location.href='https://edsurf.co.cc/browse.php?u='+encodeURIComponent(location.href)})();
    //Broken out
    javascript:(
    function(){
    javascript:location.href='https://edsurf.co.cc/browse.php?url='
    +
    encodeURIComponent(
    location.href)
    }
    )
    ();
    //Second attempt, no need for encoding =.=!
    javascript:(function(){javascript:location.href='https://edhaker.info/browse.php?u='+location.href})();
    //Broken out again
    javascript:(
    function(){
    javascript:location.href=
    'https://edsurf.co.cc/browse.php?url='+
    location.href
    }
    )
    ();
